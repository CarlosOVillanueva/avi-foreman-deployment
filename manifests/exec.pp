# == Class: avi::exec
class avi::exec inherits avi {
  # ensure presence of wget class
  require wget

  # set package
  $package_name = "${avi::package_name}-${avi::version}.tar.gz"

  # create install directory
  file { 'avi_install_dir':
    ensure => 'directory',
    path   => '/opt/avi-install',
    before => Wget::Fetch['avi_installer']
  }

  # fetch avi installer package
  wget::fetch { 'avi_installer':
    source      => "${avi::package_uri}/${package_name}",
    destination => "/opt/avi-install/${package_name}",
    timeout     => 0,
    unless      => "test -f /opt/avi-install/${package_name}",
    before      => File['avi_package'],
  }

  # validate package is present and chmod
  file { 'avi_package':
    ensure => file,
    mode   => '0600',
    path   => "/opt/avi-install/${package_name}",
    before => Exec['extract_avi_package'],
  }

  # extract package
  exec {'extract_avi_package':
    path     => '/usr/bin:/usr/sbin:/bin',
    provider => shell,
    onlyif   => [ "test -f /opt/avi-install/${package_name}",'ls /opt/avi-install | wc -l | grep -o 1' ],
    cwd      => '/opt/avi-install',
    command  => "tar xvf ${package_name}",
  }
}
