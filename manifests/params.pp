# == Class: avi::params
#
# Default parameter values for the avi module
#
class avi::params {
  $version  = undef
  $role = 'controller'
  $package_uri = undef
  $package_name = undef
}
