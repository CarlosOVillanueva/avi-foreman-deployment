# == Class: avi::config
class avi::config inherits avi {
  # exclude kernel updates from Yum
  augeas { 'yum_exclude_kernel':
    context => '/files/etc/yum.conf/main',
    changes =>  [
          'set exclude kernel*',],
  }
}
