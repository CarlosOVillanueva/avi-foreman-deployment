# == Class: avi::install
class avi::install inherits avi {
  package { 'lshw':
    ensure => installed,
  }

  package { 'net-tools':
    ensure => installed,
  }

  package { 'vim-enhanced':
    ensure => installed,
  }

  package { 'tar':
    ensure => installed,
  }

  package { 'wget':
    ensure => installed,
  }

  package { 'screen':
    ensure => installed,
  }
}
