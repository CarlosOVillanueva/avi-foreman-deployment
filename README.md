# AVI - Deployment and Configuration

#### Table of Contents
1. [Overview](#overview)
2. [Module Description](#module-description)
3. [Setup](#setup)
4. [Usage](#usage)
5. [Reference](#reference)
6. [Limitations](#limitations)
7. [Development](#development)

## Overview
This puppet class stages a target host to support AVI.

## Module Description
The manifests within this module will first install all mandatory packages (*install.pp*) then disable *kernel* upgrades (*config.pp*) and lastly pulls/extracts avi package for installation (*exec.pp*).

### install.pp
- Installs yum packages necessary for AVI to run.

### config.pp
- Disables yum kernel updates. Any service configurations should be defined within this manifest.

### exec.pp
- Requires WGET class. Pulls tar.gz avi package based on global parameters and extracts it under `/opt/avi-install`

## Setup
This module is meant to run on **Foreman** or any *Puppet Master*.

Pull the latest tar.gz file from GIT and install it using `puppet module install`.

    $ wget https://gitlab.com/CarlosOVillanueva/avi-foreman-deployment/repository/archive.tar.gz
    $ puppet module install archive.tar.gz

### Final Steps
Both **Foreman** and a Puppet Master will need to be made aware of the newly installed module. This typically consists of importing it into the system. Once imported, add it to the target host and the next time the target's *puppet agent* runs, it will pull the manifest files.

In addition, the default `params.pp` needs to be updated; otherwise, pull of the AVI installer package **will fail**.

    class avi::params {
      $version  = '17.1.2-9038'
      $role = 'controller'
      $package_uri = 'http://someurl.com'
      $package_name = 'docker_install'
    }

## Dependencies
The following dependencies are pulled automatically by the module.

- Puppetlabs Docker Platform
- Maestrodev WGET

## Reference
- [Installing Puppet Modules](https://docs.puppet.com/puppet/5.0/modules_installing.html)
- [Foreman 1.15 Reference Material](https://www.theforeman.org/manuals/1.15/index.html)

## Limitations
- Sets up controllers and service engines in the same manner; however, this will later be changed to support parameters.
